# Aedifex
*is Laten* and means as much as builder, contractor, architect, maker - depends how it's used, I guess.

## Modifeied Scripts
These scripts are based on [LFScript4](https://lfscript.org). They are heavily modded to fit the needs of DFS Linux. There may be modifications baised towards the OS we are building, be we will make all attempts to keep it disto nutrial.

## License
Same as the orinal scripts - [MIT Licensed](LICENSE).

## Docs
Not much, yet. Try the [WIKI](../../wikis/home), something might be there.

## Bugs, Issues, Requests, Support
You can do all that in the [ISSUES AREA](../../issues).

